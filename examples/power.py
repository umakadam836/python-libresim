import enum

import libresim


class ElementEvent(enum.IntEnum):
    ON_LOAD_CHANGED = 1
    ON_POWERED = 2
    ON_NOT_POWERED = 3


class PowerStatus(enum.IntEnum):
    NOT_POWERED = 0
    POWERED = 1


class ElectricalNetwork(libresim.Component, libresim.Composite):

    def __init__(self, name, description, parent):
        super().__init__(name, description, parent)

        self._delta_time = None
        self._update_event_id = None

        self._update_entrypoint = libresim.EntryPoint(
            "Update",
            "Updates temperatures of all thermal nodes in the thermal network.",
            self,
            self.update
            )

        self._containers = []


class Element(libresim.Component, libresim.EventProvider):

    def __init__(self, name, description, parent):
        super().__init__(name, description, parent)

        self._power_load = None
        self._power_status = None
        self._input_voltage = None
        self._output_voltage = None
        self._network = None

        # event sources
        self.on_load_changed = libresim.EventSource("OnLoadChanged", "", self)

    def power_on(self):
        self._power_status = True

    def power_off(self, value):
        self._power_status = False

    def publish(self):
        self.receiver.publish_field(
            "status",
            "Status of the hot object.",
            self._status)


class SimplePole(Element):

    def __init__(self, name, description, parent):
        super().__init__(name, description, parent)

        self._position = None


class PowerSource(Element):
    pass


class Terminator(Element):
    pass


class Action(libresim.Component, libresim.Aggregate, libresim.EventConsumer):

    def __init__(self, name, description, parent):
        super().__init__(name, description, parent)

        self._action_status = None
        self._locking_status = None

        self._references = []



element.on_load_changed.subscribe(action.execute)
