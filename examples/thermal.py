import libresim.generic.models.thermal as libresim


thermal_network = libresim.create_thermal_network(
    "TNET", "MyTNET", None,
    delta_time=1)

hot_objects = libresim.HotObjects("HotObjects", "", thermal_network)
HO_1 = libresim.create_hot_object(
    "HO_1", "", hot_objects,
    status=True)
hot_objects.add_component(HO_1)
thermal_network.add_container(hot_objects)

thermal_nodes = libresim.ThermalNodes("ThermalNodes", "", thermal_network)
TN_1 = libresim.create_thermal_node(
    "TN_1", "", thermal_nodes,
    base_temperature=20,
    current_temperature=25,
    rise_rate=1,
    fall_rate=1,
    offset=0,
    scale=1)
thermal_nodes.add_component(TN_1)
thermal_network.add_container(thermal_nodes)

related_hot_objects = libresim.RelatedHotObjects("RelatedHotObjects", "", TN_1)
TN_1_HO_1 = libresim.create_related_hot_object(
    "TN_1_HO_1", "", related_hot_objects,
    HO_1,
    20)
related_hot_objects.add_component(TN_1_HO_1)
TN_1.add_container(related_hot_objects)


# TODO: load from config file:
# thermal_network = libresim.ThermalNetwork.from_config(
#     "thermal_config.yaml", "TNet", "MyTNet", parent=None, delta_time=1)

root = thermal_network
