import argparse
import importlib

import libresim

# load model from external file
parser = argparse.ArgumentParser()
parser.add_argument('modelfile')
args = parser.parse_args()
model = importlib.import_module(args.modelfile)

# create simulator
simulator = libresim.Simulator()
simulator.set_time_progress(libresim.SimulationTimeProgress.REALTIME)
# simulator.set_time_progress(libresim.SimulationTimeProgress.ACCELERATED, 10)
# simulator.set_time_progress(libresim.SimulationTimeProgress.FREE_RUNNING)

# add models
simulator.add_model(model.root)

# simulator setup
simulator.publish()
simulator.configure()
simulator.connect()

# start executing simulation
print("Simulation running...")
print("Press <Enter> to stop")
print()
simulator.run()
input()

print("Simulation completed")
simulator.hold()
simulator.exit()
