import libresim


class HotObject(libresim.Component):

    """A hot object is a component which can be either ON (True) or OFF (False).

    """
    def __init__(self, name, description, parent):
        super().__init__(name, description, parent)

        self._status = None

    def get_status(self):
        return self._status

    def set_status(self, value):
        self._status = value

    def publish(self):
        self.receiver.publish_field(
            "status",
            "Status of the hot object.",
            self._status)


class HotObjects(libresim.Component, libresim.Container):
    pass
